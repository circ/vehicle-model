#include "VehicleModel.hpp"

using namespace std;
using namespace polysync;
using namespace CIRC;
namespace dm = polysync::datamodel;

/**********************************************************************
** Wheel-E Vehicle Model
** Inputs:
**	- Platform Command Curvature (1/m):  reciprocal of radius of
**	  curvature of center of robot.  Positive value is leftward turn,
**	  negative rightward, 0 is straight.
**	- Platform Command Throttle (m/s):  speed of the robot.  Positive
**	  is forward, negative reverse.
**
** Outputs:
**	- Platform Throttle Left:  throttle command for left side motors.
**	  -1.0 is full reverse, 0.0 is stopped, +1.0 is full forward.
**	- Platform Throttle Right:  throttle command for right side motors.
**	  -1.0 is full reverse, 0.0 is stopped, +1.0 is full forward.
**	- Platform Steering Left (rad):  wheel angle for left side servos.
**	  Positive is clockwise, negative counterclockwise.
**	- Platform Steering Right (rad):  wheel angle for right side servos.
**	  Positive is clockwise, negative counterclockwise.
**********************************************************************/

//Default constructor.
VehicleModel::VehicleModel(int argc, char * argv[]) :
    Node(argc, argv)
{
}

//Default destructor.
VehicleModel::~VehicleModel()
{
}

//Can handle arguments here, if using the constructor with command line arguments.
void VehicleModel::setConfigurationEvent(int argc, char * argv[])
{
    int c;
    bool exit = false;

    optind = 0; //Resets the parsing index for getopt.
    opterr = 1; //Re-enables showing an error for invalid parameters.

    while ((c = getopt(argc, argv, "hno")) != -1)
    {
        switch (c)
        {
            case 'h':
                cout << endl;
                cout << "CIRC Wheel-E PolySync 2.x Vehicle Model." << endl;
                cout << "    Usage: VehicleModel [options]" << endl;
                cout << "    -h                Show this help menu and exit." << endl;
                cout << "    -n <node_id> The SDF Node Id (only if defined in the SDF Configurator)." << endl;
                cout << "    -o                Enable PolySync logging output." << endl;
                cout << endl;
                exit = true;
            case 'n':
                break;
            case 'o':
                break;
            case '?':
                if (isprint(optopt))
                    fprintf(stderr, "Unknown option '-%c'.\n", optopt);
                else
                    fprintf(stderr, "Unknown option character '\\x%x'.\n", optopt);
                exit = true;
            default:
                abort();
        }
    }

    if (exit)
        disconnectPolySync();
}


//Register listeners for message types here.
void VehicleModel::initStateEvent()
{
    if (enableLogging)
        setFlags(0 | PSYNC_INIT_FLAG_STDOUT_LOGGING);
    else
        setFlags(0);

    //How to register message listeners.
    
    ps_msg_type type = getMessageTypeByName("ps_platform_control_msg");
    registerListener(type);
}

//Clean up resources in this function.
void VehicleModel::releaseStateEvent()
{
    // Always executed before the node shuts down
}

void VehicleModel::errorStateEvent()
{
}

void VehicleModel::fatalStateEvent()
{
    // Run once
}

void VehicleModel::warnStateEvent()
{
}

//Add time-dependent message publishing here.
//Node enters this state on every loop if not in another state.
void VehicleModel::okStateEvent()
{
    this_thread::sleep_for(chrono::milliseconds(20));
}

//Handle incoming messages here. This is handled in a
//separate node from the main thread and is therefore non-blocking.
void VehicleModel::messageEvent(shared_ptr<Message> message)
{
	float speed, curvature;	// From platform control message
	float throttleLeft, throttleRight;	// Published values
	float steerLeft, steerRight;		// Published values

    //Example of message filtering.
    if (auto pcMsg = datamodel::getSubclass<datamodel::PlatformControlMessage>(message))
    {
//        pcMsg->print();
		if (pcMsg->getSensorDescriptorId() == DESCRIPTOR_BOTBOARD)
		{
			speed = pcMsg->getSpeed();
			curvature = pcMsg->getCurvature();
			calculateSteerThrottle(
				speed, curvature,
				&throttleLeft, &throttleRight, &steerLeft, &steerRight);

			// Publish platform steering and throttle commands
			datamodel::SensorDescriptor sd;

			sd.setId(DESCRIPTOR_VEHICLE_MODEL_LEFT);
			sd.setType(PSYNC_SENSOR_KIND_NOT_AVAILABLE);
			datamodel::PlatformSteeringCommandMessage psMsgLeft(*this);
			psMsgLeft.setSensorDescriptor(sd);
			psMsgLeft.setHeaderTimestamp(getTimestamp());
			psMsgLeft.setDestGuid(PSYNC_GUID_INVALID);
			psMsgLeft.setEnabled(1);
			psMsgLeft.setSteeringCommandKind(STEERING_COMMAND_ANGLE);
			psMsgLeft.setSteeringWheelAngle(steerLeft);
			psMsgLeft.setMaxSteeringWheelRotationRate(0);
			psMsgLeft.setTimestamp(getTimestamp());
			psMsgLeft.publish();

			sd.setId(DESCRIPTOR_VEHICLE_MODEL_LEFT);
			sd.setType(PSYNC_SENSOR_KIND_NOT_AVAILABLE);
			datamodel::PlatformThrottleCommandMessage ptMsgLeft(*this);
			ptMsgLeft.setSensorDescriptor(sd);
			ptMsgLeft.setHeaderTimestamp(getTimestamp());
			ptMsgLeft.setDestGuid(PSYNC_GUID_INVALID);
			ptMsgLeft.setEnabled(1);
			ptMsgLeft.setThrottleCommandType(THROTTLE_COMMAND_PERCENT);
			ptMsgLeft.setThrottleCommand(throttleLeft);
			ptMsgLeft.setTimestamp(getTimestamp());
			ptMsgLeft.publish();
    this_thread::sleep_for(chrono::milliseconds(1));

			sd.setId(DESCRIPTOR_VEHICLE_MODEL_RIGHT);
			sd.setType(PSYNC_SENSOR_KIND_NOT_AVAILABLE);
			datamodel::PlatformSteeringCommandMessage psMsgRight(*this);
			psMsgRight.setSensorDescriptor(sd);
			psMsgRight.setHeaderTimestamp(getTimestamp());
			psMsgRight.setDestGuid(PSYNC_GUID_INVALID);
			psMsgRight.setEnabled(1);
			psMsgRight.setSteeringCommandKind(STEERING_COMMAND_ANGLE);
			psMsgRight.setSteeringWheelAngle(steerRight);
			psMsgRight.setMaxSteeringWheelRotationRate(0);
			psMsgRight.setTimestamp(getTimestamp());
			psMsgRight.publish();

			sd.setId(DESCRIPTOR_VEHICLE_MODEL_RIGHT);
			sd.setType(PSYNC_SENSOR_KIND_NOT_AVAILABLE);
			datamodel::PlatformThrottleCommandMessage ptMsgRight(*this);
			ptMsgRight.setSensorDescriptor(sd);
			ptMsgRight.setHeaderTimestamp(getTimestamp());
			ptMsgRight.setDestGuid(PSYNC_GUID_INVALID);
			ptMsgRight.setEnabled(1);
			ptMsgRight.setThrottleCommandType(THROTTLE_COMMAND_PERCENT);
			ptMsgRight.setThrottleCommand(throttleRight);
			ptMsgRight.setTimestamp(getTimestamp());
			ptMsgRight.publish();
		}
    }
}

//*********************************************************************
// Calculate the steering and throttle commands based on the speed
// and curvature passed.
//*********************************************************************
void VehicleModel::calculateSteerThrottle(
	float speed, float curvature,	// Inputs
	float *throttleLeft, float *throttleRight,	// Outputs
	float *steerLeft, float *steerRight)
{
	float speedLeft;
	float speedRight;
	float absThrottleLeft;
	float absThrottleRight;
	float maxAbsThrottle;
	float throttleMultiplier;

	// Caclulate left/right wheel speeds in m/s
	speedLeft = speed * sqrt(pow(curvature * WHEELBASE_LENGTH/2, 2) + pow(1 - curvature * WHEELBASE_WIDTH / 2, 2));
	speedRight = speed * sqrt(pow(curvature * WHEELBASE_LENGTH/2, 2) + pow(1 + curvature * WHEELBASE_WIDTH / 2, 2));

	// Calculate left/right wheel angles in radians
	*steerLeft = atan(curvature * WHEELBASE_LENGTH / (2 - WHEELBASE_WIDTH));
	*steerRight = atan(curvature * WHEELBASE_LENGTH / (2 + WHEELBASE_WIDTH));

	// Normalize wheel speeds to obtain left/right throttle values
	*throttleLeft = speedLeft / FULL_THROTTLE_SPEED;
	*throttleRight = speedRight / FULL_THROTTLE_SPEED;

	// Limit throttle to +/- 1.0, keeping the ratio left/right constant to maintain curvature
	absThrottleLeft = fabs(*throttleLeft);
	absThrottleRight = fabs(*throttleRight);
	maxAbsThrottle = (absThrottleLeft > absThrottleRight) ? absThrottleLeft : absThrottleRight;
	throttleMultiplier = (maxAbsThrottle > 1.0) ? (1.0 / maxAbsThrottle) : 1.0;
	*throttleLeft *= throttleMultiplier;
	*throttleRight *= throttleMultiplier;
}
