# Wheel-E Vehicle Model node for PolySync 2.x with C++ #

Receives speed and curvature commands, then generates right/left steer and throttle commands based on Wheel-E geometry.