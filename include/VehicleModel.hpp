#ifndef VehicleModel_HPP
#define VehicleModel_HPP

//C++ Includes
#include <iostream>
#include <thread>
#include <chrono>

//OS Includes
#include <unistd.h>

//PolySync Includes
#include <PolySyncNode.hpp>
#include <PolySyncMessage.hpp>
#include <PolySyncDataModel.hpp>

namespace CIRC
{
    class VehicleModel : public polysync::Node
    {
        public:
	    VehicleModel(int argc, char * argv[]);
	    ~VehicleModel();
        private:
	    void setConfigurationEvent(int argc, char * argv[]) override;
	    void initStateEvent() override;
	    void warnStateEvent() override;
	    void errorStateEvent() override;
	    void fatalStateEvent() override;
	    void okStateEvent() override;
	    void releaseStateEvent() override;
	    void messageEvent(std::shared_ptr<polysync::Message> message) override;
		void calculateSteerThrottle(
			float speed, float curvature,
			float *throttleLeft, float *throttleRight,
			float *steerLeft, float *steerRight);
	    bool enableLogging = false;
		// Wheel-E geometry constants
		const float WHEELBASE_WIDTH = (23 * 2.54 / 100);		// Meters
		const float WHEELBASE_LENGTH = (12.5 * 2.54 / 100);	// Meters
		const float FULL_THROTTLE_SPEED = 8.0;	// Speed in m/s when throttle is 1.0
    };
}

#endif
